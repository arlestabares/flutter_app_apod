part of 'apod_app_bloc.dart';
abstract class ApodAppState extends Equatable {
  const ApodAppState();
}
class ApodAppInitial extends ApodAppState {
  @override
  List<Object> get props => [];
}
