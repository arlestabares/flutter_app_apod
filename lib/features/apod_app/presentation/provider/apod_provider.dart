import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter_apod_app/features/apod_app/data/models/apod_model.dart';

//
class ApodProvider with ChangeNotifier {
  DateTime? date = DateTime.now();

  List<Apod> _globalList = [];

  List<Apod> get globalList => _globalList;

  get year => date!.year.toString();
  get month => date!.month.toString();
  get day => date!.day.toString();
  //"https://api.nasa.gov/planetary/apod?date=$dateString";
  // String url = 'https://api.nasa.gov/planetary/apod?count=5&api_key=DEMO_KEY&date=${date}';
  String getDateString() => "$year-$month-$day";

  static const String apikey = 'Cs4rkWXt1gdKNqlT5RJB9uMLpbnxaGdxRHKcaPHk';
  static const String apikey2 = 'DEMO_KEY';
  final _dio = Dio();

  //apikey = Cs4rkWXt1gdKNqlT5RJB9uMLpbnxaGdxRHKcaPHk
  //ejemplo:
  //https://api.nasa.gov/planetary/apod? api_key = Cs4rkWXt1gdKNqlT5RJB9uMLpbnxaGdxRHKcaPHk
  String url(DateTime dateSearch) =>
      "https://apodapi.herokuapp.com/api/?date=$dateSearch&apy_key=$apikey";

  Future<Apod> getApod(DateTime date) async {
    Response response = await _dio.get(url(date));
    return Apod.fromJson(response.data);
  }

  Future<List<Apod>> getApodImage() async {
     String url = 'https://api.nasa.gov/planetary/apod?count=10&api_key=DEMO_KEY';
    String dataString = getDateString();
    // const String url = 'https://api.nasa.gov/apod?date=&api_key=$apikey';
    // const String url2 = 'https://apodapi.herokuapp.com/api/?count=10';
    // const String url2 = 'https://api.nasa.gov/planetary/apod?count=10&api_key=$apikey';
    try {
      final response = await _dio.get(url);

      final res =
          ((response.data) as List).map((e) => Apod.fromJson(e)).toList();
          
       _globalList = res;
       notifyListeners();
       print('valor de lista: $_globalList');
       return res;
    } catch (e) {
      // print(e);
      throw Exception('Algo salio mal');
    }
  }
}
