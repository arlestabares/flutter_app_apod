import 'package:flutter/material.dart';
import 'package:flutter_apod_app/features/apod_app/presentation/pages/apod_page.dart';

final Map<String, Widget Function(BuildContext)> appRoutes = {
  'apodPage': (_) => const ApodPage(),
  'staggeredGridViewPrint': (_) => const StaggeredGridViewPrint(),
};
