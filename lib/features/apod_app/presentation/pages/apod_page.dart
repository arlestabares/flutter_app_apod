import 'package:animations/animations.dart';
import 'package:flutter/material.dart';
import 'package:flutter_apod_app/features/apod_app/data/models/apod_model.dart';
import 'package:flutter_apod_app/features/apod_app/presentation/provider/apod_provider.dart';
import 'package:flutter_apod_app/features/apod_app/presentation/widgets/card_details_apod.dart';
import 'package:flutter_apod_app/features/apod_app/presentation/widgets/card_widget.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:provider/provider.dart';

class ApodPage extends StatefulWidget {
  const ApodPage({Key? key}) : super(key: key);

  @override
  State<ApodPage> createState() => _ApodPageState();
}

class _ApodPageState extends State<ApodPage> {
  @override
  Widget build(BuildContext context) {
    //
    final p = Provider.of<ApodProvider>(context);
    //
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.black,
        centerTitle: true,
        elevation: 16.0,
        title: const Text(
          'Apod Nasa',
          style: TextStyle(
            fontSize: 24.0,
            fontWeight: FontWeight.bold,
          ),
        ),
        actions: <Widget>[
          IconButton(
            icon: const Icon(Icons.autorenew),
            onPressed: () {
              Provider.of<ApodProvider>(context, listen: false).getApodImage();
              setState(() {});
            },
          )
        ],
      ),
      body: const Center(),
    );
  }
}

// class FututeBuilderListView extends StatelessWidget {
//   const FututeBuilderListView({
//     Key? key,
//   }) : super(key: key);

//   @override
//   Widget build(BuildContext context) {
//     final apodProvider = Provider.of<ApodProvider>(context).getApodImage();
//     return FutureBuilder(
//       future: apodProvider,
//       builder: (context, AsyncSnapshot<dynamic> snapshot) {
//         final apod = snapshot.data;
//         if (snapshot.hasData) {
//           return ListView.builder(
//             itemCount: apod.length,
//             itemBuilder: (context, i) {
//               return ListTile(
//                 title: Text(apod[i].title!),
//                 subtitle: Text(apod[i].hdurl!),
//               );
//             },
//           );
// children: [
//   snapshot.data.map(() => CardApod(apod: apod)),
// ],

//         } else if (snapshot.hasError) {
//           return Center(child: Text('${snapshot.hasError}'));
//         } else {
//           return const Center(
//             child: CircularProgressIndicator(),
//           );
//         }
//       },
//     );
//   }
// }

Container _buildApodImage(Apod apod) {
  return Container(
    padding: const EdgeInsets.fromLTRB(20, 0, 20, 0),
    child: ClipRRect(
      borderRadius: BorderRadius.circular(6.0),
      child: Container(
        child: Image.network(''),
      ),
    ),
  );
}

class StaggeredGridViewPrint extends StatefulWidget {
  const StaggeredGridViewPrint({Key? key}) : super(key: key);

  @override
  State<StaggeredGridViewPrint> createState() => _StaggeredGridViewPrintState();
}

class _StaggeredGridViewPrintState extends State<StaggeredGridViewPrint> {
 
 bool istrue = false;
  @override
  void initState() {
    super.initState();
    final p = Provider.of<ApodProvider>(context,listen: false);
    p.getApodImage();

  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: _buildAppBar(context),
      body: _staggeredGridView(),
    );
  }

  StaggeredGridView _staggeredGridView() {
    final provider = Provider.of<ApodProvider>(context);

    return StaggeredGridView.countBuilder(
      itemCount: provider.globalList.length,
      crossAxisCount: 4,
      mainAxisSpacing: 4,
      crossAxisSpacing: 4,
      itemBuilder: (context, index) => OpenContainer(
        openBuilder: (context, _) => DetailsWidget(index: index),
        closedBuilder: (context, VoidCallback openContainer) =>
            CardWidget(index: index, openContainer: openContainer),
      ),
      staggeredTileBuilder: (index) {
        const crossAxisCellCount = 2;
        final mainAxisCellCount = index.isEven ? 3 : 2;

        return StaggeredTile.count(
            crossAxisCellCount, mainAxisCellCount.toDouble());
      },
    );
  }

  AppBar _buildAppBar(BuildContext context) {
    return AppBar(
      backgroundColor: Colors.black.withOpacity(0.4),
      centerTitle: true,
      elevation: 16.0,
      title: const Text(
        'Apod Nasa',
        style: TextStyle(
          fontSize: 16.0,
          fontWeight: FontWeight.bold,
        ),
      ),
      actions: <Widget>[
        IconButton(
          icon: const Icon(Icons.autorenew),
          onPressed: () {
            final p = Provider.of<ApodProvider>(context, listen: false);
            p.getApodImage;
            setState(() {});
          },
        )
      ],
    );
  }
}
