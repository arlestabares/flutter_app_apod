import 'package:flutter/material.dart';
import 'package:flutter_apod_app/features/apod_app/presentation/provider/apod_provider.dart';
import 'package:provider/provider.dart';

class CardWidget extends StatelessWidget {
  final int? index;
  final VoidCallback? openContainer;

  const CardWidget({
    Key? key,
    this.index,
    this.openContainer,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final p = Provider.of<ApodProvider>(context, listen: false).globalList;

    // const url = 'https://api.nasa.gov/planetary/apod?count=5&api_key=DEMO_KEY';
    return InkWell(
      child: Column(
        children: [
          Expanded(
            child: SizedBox(
              child: Image.network(
                p[index!].hdurl!,
                fit: BoxFit.cover,
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Text(
              p[index!].title!,
              style: const TextStyle(
                fontSize: 18.0,
              ),
            ),
          )
        ],
      ),
    );
  }
}
