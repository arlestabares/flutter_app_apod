import 'package:flutter/material.dart';
import 'package:flutter_apod_app/features/apod_app/data/models/apod_model.dart';
import 'package:flutter_apod_app/features/apod_app/presentation/provider/apod_provider.dart';
import 'package:provider/provider.dart';

class DetailsWidget extends StatelessWidget {
  final Apod? apod;
  final int? index;
  const DetailsWidget({Key? key, this.apod, this.index}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final p = Provider.of<ApodProvider>(context,listen: false).globalList;
    return Scaffold(
      appBar: AppBar(
        title: const Text('DetailsPage'),
      ),
      body: InkWell(
        onTap: () => Navigator.pop(context),
        child: ListView(
          children: <Widget>[
            SizedBox(
              height: 300.0,
              child:  Image.network(
                      p[index!].hdurl!,
                      fit: BoxFit.cover,
                    ),
            ),
            Column(
              children: [
                Text(
                  'Nasa Image',
                  style: Theme.of(context)
                      .textTheme
                      .headline5!
                      .copyWith(color: Colors.black54, fontSize: 30.0),
                ),
                const SizedBox(height: 20.0),
                Text(
                  p[index!].explanation!,
                  style: const TextStyle(
                    color: Colors.black54,
                    height: 1.5,
                    fontSize: 16.0,
                  ),
                )
              ],
            )
          ],
        ),
      ),
    );
  }
}
