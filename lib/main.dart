import 'package:flutter/material.dart';
import 'package:flutter_apod_app/features/apod_app/presentation/provider/apod_provider.dart';
import 'package:flutter_apod_app/features/apod_app/presentation/routes/routes.dart';
import 'package:provider/provider.dart';

void main() => runApp(const MyApp());

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [ChangeNotifierProvider(create: (_) => ApodProvider())],
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        title: 'Material App',
        // initialRoute: 'apodPage',
        initialRoute: 'staggeredGridViewPrint',
        routes: appRoutes,
      ),
    );
  }
}
